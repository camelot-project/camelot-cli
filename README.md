# Camelot CLI

CLI tool for interacting with Camelot.

Currently does not do much. See the [design document](doc/design.org) for the vision.

## License

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
