(ns camelot.cli.service.registry
  (:require
   [clojure.string :as str]
   [goog.string :as gstring]
   [goog.string.format]))

(defprotocol Registration
  (info [this]
    "Get the details of a registration")
  (run [this args]
    "Run the given registration"))

(defn list-available-registrations!
  [registry]
  (let [registry-info (sort :id (map info registry))]
    (doseq [{:keys [id description]} registry-info]
      (print (gstring/format "  %-20s\t%s" id description)))))

(defn run-from-registry
  [reg [key & rem-args]]
  (if-let [runnable (first (filter (comp #(= key %) :id info) reg))]
    (run runnable rem-args)
    (binding [*print-fn* *print-err-fn*]
      (println (str
                "Not found: " key ". Should be one of: "
                (str/join ", " (map (comp :id info) reg)))))))

(defn operation-usage
  ([module-name operation-name cli]
   (binding [*print-fn* *print-err-fn*]
     (println "Usage: camelot-cli" module-name operation-name "[<options>]")
     (print "\nOptions:")
     (println (:summary cli))))
  ([module-name operation-name arg cli]
   (binding [*print-fn* *print-err-fn*]
     (println "Usage: camelot-cli" module-name operation-name "<" arg ">" "[<options>]")
     (print "\nOptions:")
     (println (:summary cli)))))

(defn module-usage
  [module-name registry cli]
  (binding [*print-fn* *print-err-fn*]
    (println "Usage: camelot-cli" module-name "[<options>] <operation>")
    (print "\nOptions:")
    (println (:summary cli))
    (print "\nAvailable operations:")
    (list-available-registrations! registry)))

(defn usage
  [registry cli]
  (binding [*print-fn* *print-err-fn*]
    (println "Usage: camelot-cli [<options>] <module>")
    (print "\nOptions:")
    (println (:summary cli))
    (print "\nAvailable modules:")
    (list-available-registrations! registry)))
