(ns camelot.cli.service.config)

(defprotocol ConfigStore
  (read [this]
    "Retrieve the config"))
