(ns camelot.cli.service.camelot-client)

(defprotocol Client
  (request [this method url ch options]
    "Make a request"))

(defn dataset-list
  [client ch]
  (request client :get "/dataset" ch {}))

(defn dataset-connect
  [client ch dataset]
  (request client :post (str "/dataset/" dataset "/connect") ch {}))

(defn dataset-disconnect
  [client ch dataset]
  (request client :post (str "/dataset/" dataset "/disconnect") ch {}))

(defn dataset-reload
  [client ch]
  (request client :post (str "/dataset/reload") ch {}))
