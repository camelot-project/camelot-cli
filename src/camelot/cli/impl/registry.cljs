(ns camelot.cli.impl.registry
  (:require [integrant.core :as ig]))

(defmethod ig/init-key :cli/registry [_ {:keys [modules]}]
  modules)
