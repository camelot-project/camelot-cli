(ns camelot.cli.impl.market-store
  "Component for the camelot-market configuration store."
  (:require
   [camelot.market.config :as market-config]
   [camelot.cli.service.config :as config]
   [integrant.core :as ig]))

(defrecord MarketStore [config-obj]
  config/ConfigStore
  (read [_]
    config-obj))

(defmethod ig/init-key :cli/config-store [_ _]
  (MarketStore. (market-config/read-config)))
