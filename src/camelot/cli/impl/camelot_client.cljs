(ns camelot.cli.impl.camelot-client
  "HTTP client for Camelot's API."
  (:require [camelot.cli.service.camelot-client :as camelot-client]
            [camelot.cli.service.config :as config]
            [httpurr.client.node :refer [client]]
            [httpurr.client :as http]
            [httpurr.status :as status]
            [promesa.core :as p]
            [cljs.core.async :as async :refer-macros [go]]
            [integrant.core :as ig]))

(def protocol "http://")
(def hostname "localhost")
(def api-base "/api/v1")
(def default-timeout 5000)

(defn- decode
  [buffer]
  (if (>= (.-length buffer) 2)
    (js->clj (js/JSON.parse buffer) :keywordize-keys true)
    {}))

(defn- handle-http-error
  [err]
  (binding [*print-fn* *print-err-fn*]
    (println (str "HTTP error: '" (.toString (:body err)) "' (status code: " (:status err) ")"))))

(defn- handle-success
  [ch resp]
  (if (status/success? resp)
    (go (async/>! ch (decode (:body resp))))
    (do
      (handle-http-error resp)
      (go (async/>! ch {::error resp})))))

(defn- handle-error
  [err]
  (binding [*print-fn* *print-err-fn*]
    (println "Error: " (.-message err))))

(defrecord CamelotClient [config-store]
  camelot-client/Client
  (request [_ method url ch options]
    (let [http-port (-> config-store config/read :server :http-port)
          http-opts (merge {:timeout default-timeout}
                           options
                           {:method method
                            :url (str protocol hostname ":" http-port
                                      api-base url)})]
      (-> (http/send! client http-opts)
          (p/then #(handle-success ch %))
          (p/catch handle-error)))
    nil))

(defmethod ig/init-key :cli/camelot-client [_ {:keys [config-store]}]
  (CamelotClient. config-store))
