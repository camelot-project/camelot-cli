(ns camelot.cli.impl.module.dataset.core
  (:require
   [camelot.cli.impl.module.dataset.operation.list :as list]
   [camelot.cli.impl.module.dataset.operation.connect :as connect]
   [camelot.cli.impl.module.dataset.operation.disconnect :as disconnect]
   [camelot.cli.impl.module.dataset.operation.reload :as reload]
   [camelot.cli.service.registry :as registry]
   [clojure.tools.cli :as cli]
   [integrant.core :as ig]))

(def component-id ::component)
(derive component-id :cli/module)

(def cli-options
  [["-h" "--help" "Usage info"]])

(def info {:id "dataset"
           :name "Dataset"
           :description "Simple dataset module"})

(defn subsystem
  []
  (hash-map
   list/component-id {:camelot-client (ig/ref :cli/camelot-client)}
   connect/component-id {:camelot-client (ig/ref :cli/camelot-client)}
   disconnect/component-id {:camelot-client (ig/ref :cli/camelot-client)}
   reload/component-id {:camelot-client (ig/ref :cli/camelot-client)}))

(defrecord DatasetModule [operations]
  registry/Registration
  (info [_] info)
  (run [this args]
    (let [cli (cli/parse-opts args cli-options :in-order true :strict true)]
      (if (-> cli :options :help)
        (registry/module-usage (:id info) operations cli)
        (registry/run-from-registry operations (:arguments cli))))))

(defmethod ig/init-key component-id [_ {:keys [operations]}]
  (DatasetModule. operations))
