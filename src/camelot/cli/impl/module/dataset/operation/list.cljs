(ns camelot.cli.impl.module.dataset.operation.list
  (:require
   [camelot.cli.service.registry :as registry]
   [camelot.cli.service.camelot-client :as camelot-client]
   [cljs.core.async :as async :refer-macros [go]]
   [clojure.tools.cli :as cli]
   [integrant.core :as ig]))

(def component-id ::component)
(derive component-id :dataset/operation)

(def info {:id "list"
           :name "List"
           :module "dataset"
           :description "List the defined datasets"})

(def cli-options
  [["-h" "--help" "Usage info"]])

(defrecord DatasetListOperation [camelot-client]
  registry/Registration
  (info [_] info)
  (run [_ args]
    (let [cli (cli/parse-opts args cli-options :in-order true :strict true)]
      (if (-> cli :options :help)
        (registry/operation-usage (:module info) (:id info) cli)
        (let [ch (async/chan)]
          (camelot-client/dataset-list camelot-client ch)
          (go
            (let [resp (async/<! ch)]
              (when-not (::camelot-client/error resp)
                (doall (map (comp println :id) (:data resp))))
              (async/close! ch)))
          nil)))))

(defmethod ig/init-key component-id [_ {:keys [camelot-client]}]
  (DatasetListOperation. camelot-client))
