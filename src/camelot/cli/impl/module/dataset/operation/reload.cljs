(ns camelot.cli.impl.module.dataset.operation.reload
  "Reload datasets."
  (:require
   [camelot.cli.service.camelot-client :as camelot-client]
   [camelot.cli.service.registry :as registry]
   [cljs.core.async :as async :refer-macros [go]]
   [clojure.tools.cli :as cli]
   [integrant.core :as ig]))

(def component-id ::component)
(derive component-id :dataset/operation)

(def info {:id "reload"
           :name "Reload"
           :module "dataset"
           :description "Reload the given dataset"})

(def cli-options
  [["-h" "--help" "Usage info"]])

(defrecord DatasetReloadOperation [camelot-client]
  registry/Registration
  (info [_] info)
  (run [_ args]
    (let [cli (cli/parse-opts args cli-options :in-order true :strict true)]
      (if (-> cli :options :help)
        (registry/operation-usage (:module info) (:id info) cli)
        (let [ch (async/chan)]
          (camelot-client/dataset-reload camelot-client ch)
          (go
            (let [_ (async/<! ch)]
              (async/close! ch)))
          nil)))))

(defmethod ig/init-key component-id [_ {:keys [camelot-client]}]
  (DatasetReloadOperation. camelot-client))
