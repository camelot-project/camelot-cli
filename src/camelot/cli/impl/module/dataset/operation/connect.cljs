(ns camelot.cli.impl.module.dataset.operation.connect
  "Connect to a dataset."
  (:require
   [camelot.cli.service.camelot-client :as camelot-client]
   [camelot.cli.service.registry :as registry]
   [cljs.core.async :as async :refer-macros [go]]
   [clojure.tools.cli :as cli]
   [integrant.core :as ig]))

(def component-id ::component)
(derive component-id :dataset/operation)

(def info {:id "connect"
           :name "Connect"
           :module "dataset"
           :description "Connect to the given dataset"})

(def cli-options
  [["-h" "--help" "Usage info"]])

(defrecord DatasetConnectOperation [camelot-client]
  registry/Registration
  (info [_] info)
  (run [_ args]
    (let [cli (cli/parse-opts args cli-options :in-order true :strict true)]
      (if (-> cli :options :help)
        (registry/operation-usage (:module info) (:id info) "dataset_id" cli)
        (let [datasets (:arguments cli)
              ch (async/chan)]
          (if (> (count datasets) 1)
            (registry/operation-usage (:module info) (:id info) "dataset_id" cli)
            (do
              (camelot-client/dataset-connect camelot-client ch (first datasets))
              (go
                (let [_ (async/<! ch)]
                  (async/close! ch)))
              nil)))))))

(defmethod ig/init-key component-id [_ {:keys [camelot-client]}]
  (DatasetConnectOperation. camelot-client))
