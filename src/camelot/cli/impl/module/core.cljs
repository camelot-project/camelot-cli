(ns camelot.cli.impl.module.core
  (:require
   [integrant.core :as ig]
   [camelot.cli.impl.module.dataset.core :as dataset]))

(defn subsystem
  []
  (merge
   (hash-map dataset/component-id {:operations (ig/refset :dataset/operation)})
   (dataset/subsystem)))
