(ns camelot.cli.impl.module.test
  (:require
   [camelot.cli.service.registry :as registry]
   [integrant.core :as ig]))

(def component-id ::component)
(derive component-id :cli/module)

(defrecord TestModule []
  registry/Registration
  (info [_]
    {:id "test"
     :name "Test"
     :description "Simple test module"})
  (run [_ args]
    (println "Test received args:" args)))

(defmethod ig/init-key component-id [_ {:keys []}]
  (TestModule.))
