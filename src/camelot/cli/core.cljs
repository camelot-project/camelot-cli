(ns camelot.cli.core
  "CLI entry point."
  (:require
   [integrant.core :as ig]
   [camelot.cli.service.registry :as registry]
   [camelot.cli.impl.registry]
   [camelot.cli.impl.camelot-client]
   [camelot.cli.impl.market-store]
   [camelot.cli.impl.module.core :as module]
   [clojure.tools.cli :as cli]))

(def cli-options
  [["-h" "--help" "Usage info"]])

(defn config
  []
  (merge {:cli/registry {:modules (ig/refset :cli/module)}
          :cli/camelot-client {:config-store (ig/ref :cli/config-store)}
          :cli/config-store {}}
         (module/subsystem)))

(defn parse-cli
  [args]
  (let [cli (cli/parse-opts args cli-options :in-order true :strict true)
        system (ig/init (config))
        reg (:cli/registry system)]
    (if (-> cli :options :help)
      (registry/usage reg cli)
      (when (>= (count (:arguments cli)) 1)
        (registry/run-from-registry reg (:arguments cli))))))

(defn -main
  [& args]
  (enable-console-print!)
  (parse-cli args))

(set! *main-cli-fn* -main)
